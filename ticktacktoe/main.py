import sys
import pygame

pygame.init()

screen = pygame.display.set_mode((128*3, 128*3))
pygame.display.set_caption("XO")
pygame.display.set_icon(pygame.image.load("slike/tic-tac-toe.png"))

tabla = [[1, 0, 0],
         [0, 0, 0],
         [0, 0, 0]]

prazno_polje = pygame.image.load("slike/prazno_polje.png")
o_polje = pygame.image.load("slike/o_polje.png")
x_polje = pygame.image.load("slike/x_polje.png")

polja = [prazno_polje, o_polje, x_polje]
polje = pygame.rect.Rect(0, 0, 128, 128)

potez = 0
igrac = 0
kraj = False

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit(0)
        elif event.type == pygame.MOUSEBUTTONDOWN and kraj == False:
            for i, red in enumerate(tabla):
                for j, _ in enumerate(red):
                    polje.x = j*polje.width
                    polje.y = i*polje.height
                    if polje.collidepoint(pygame.mouse.get_pos()):
                        potez += 1
                        igrac = potez%2 + 1
                        tabla[i][j] = igrac

    # kraj = True
    for i, red in enumerate(tabla):
        for j, kolona in enumerate(red):
            # if kolona == 0:
            #     kraj = False
            polje.x = j*polje.width
            polje.y = i*polje.height
            screen.blit(polja[kolona], polje)

    pygame.display.flip()