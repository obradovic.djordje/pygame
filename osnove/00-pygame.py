import pygame


pygame.init()

screen = pygame.display.set_mode((800,600))
done = False

x, y = (50, 50)
clock = pygame.time.Clock()

img = pygame.image.load('ball.png')

font = pygame.font.SysFont("Amiko", 20)
text = font.render("Singidunum", True, (100,100,100))

points = []
boja = (0,0,255)


while not done:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
    pressed = pygame.key.get_pressed()

    if pressed[pygame.K_UP]:
        y -= 3
    if pressed[pygame.K_DOWN]:
        y += 3
    if pressed[pygame.K_LEFT]:
        x -= 3
    if pressed[pygame.K_RIGHT]:
        x += 3
        if x > 800:
            x = 0
    if pressed[pygame.K_b]:
        boja = (0, 0, 255)
    if pressed[pygame.K_c]:
        boja = (255, 0, 0)
    if pressed[pygame.K_s]:
        with open("points.txt", "w") as file:
            for el, bb in points:
                file.write("{}, {}\n".format(el, bb))


    if pressed[pygame.K_SPACE]:
        points.append(((x, y), boja))

    screen.fill((50, 50, 50))


    for i in range(20):
        pygame.draw.line(screen, (100, 100, 100), (50*i, 0), (50*i, 600), 1)    

    for i in range(40):
        pygame.draw.line(screen, (100, 100, 100), (400, 300), (20*i, 600), 1)

    pygame.draw.circle(screen, (0, 200, 0), (x,y), 50, 3)
    pygame.draw.line(screen, (255, 0, 255), (0, 600), (800, 0), 3)
    pygame.draw.rect(screen, (255, 0, 0), pygame.Rect(100, 100, 60, 60))

    screen.blit(text, (50, 200))
    screen.blit(img, (200, 300))

    for el, bb in points:
        pygame.draw.circle(screen, bb, el, 3)


    pygame.display.flip()
    clock.tick(100)